from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)


author = 'Your name here'

doc = """
Example Quadratic Voting
"""


class Constants(BaseConstants):
    total_credits = 100
    questions = [
        {'label': "First Question", 'name': 'q1'},
        {'label': "Second Questions", 'name': 'q2'},
        {'label': "Third Question", 'name': 'q3'},
        {'label': "Fourth Question", 'name': 'q4'},
        {'label': "Fifth Question", 'name': 'q5'},
        {'label': "Sixth Question", 'name': 'q6'},
        {'label': "Seventh Question", 'name': 'q7'},
    ]
    name_in_url = 'quadratic_survey'
    players_per_group = None
    num_rounds = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    for question in Constants.questions:
        locals()[question['name']] = models.IntegerField(label=question['label'], min=-10, max=10, initial=0)
    del question


def custom_export(players):
    questions_names = []
    for q in Constants.questions:
        questions_names.append(q['name'])

    yield ['session', 'participant_code', 'current_page_name', 'time_started'] + questions_names
    for p in players.filter(participant__visited=True):
        answers = []
        for q in questions_names:
            answers.append(getattr(p, q))
        yield [p.session.code, p.participant.code, p.participant._current_page_name, p.participant.time_started] + answers

