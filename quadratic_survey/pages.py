from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants
import random


def shuffled_questions():
    questions_array = Constants.questions.copy()
    random.shuffle(questions_array)
    return questions_array


class IntroPage(Page):
    pass


class SurveyPage(Page):
    form_model = 'player'
    form_fields = []

    for question in Constants.questions:
        form_fields.append(str(question['name']))

    def vars_for_template(self):
        return {
            'questions': shuffled_questions(),
            'total_votes': Constants.total_credits
        }

    def js_vars(self):
        return dict(
            totalVotes=Constants.total_credits
        )

    def error_message(self, values):
        credits_sum = 0
        for v in list(values.values()):
            credits_sum += v ** 2

        if credits_sum > Constants.total_credits:
            return "Invalid values"
        elif credits_sum == 0:
            return 'You have to spend at least one credit'


class EndPage(Page):
    pass


page_sequence = [IntroPage, SurveyPage, EndPage]
