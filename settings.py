from os import environ

SESSION_CONFIGS = [
    dict(
        name='quadratic_voting',
        num_demo_participants=1,
        app_sequence=['quadratic_survey'],
        survey_title="Survey",
        display_name="Quadratic Voting"
    )
]

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=1.00, participation_fee=0.00, doc=""
)

LANGUAGE_CODE = 'en'

REAL_WORLD_CURRENCY_CODE = 'USD'
USE_POINTS = True

ROOMS = []

ADMIN_USERNAME = 'admin'
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

DEMO_PAGE_INTRO_HTML = """ """

SECRET_KEY = '_#l=*ujgpp(_zf1uia0n-7h!83^qm=b41_vu=3wmu(#36sq$ob'

INSTALLED_APPS = ['otree']
